import itertools as its

# words = r'1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM~!@#$%^&*()_+=-`{}|[]\<>?,./:";'
#
number_words = '1234567890'

# 生成3位密码，repeat=密码位数
# product函数，全排列word，排列位数repeat

# 三位数密码
# r = its.product(words, repeat=3)
# dic = open("pass3.txt", 'a')

# 八位数密码
# r = its.product(words, repeat=8)
# dic = open("pass8.txt", 'a')

#  三位纯数字密码
r = its.product(number_words, repeat=3)
dic = open("number_pass3.txt", 'a')

for i in r:
    dic.write("".join(i))
    dic.write("".join('\n'))
dic.close()
