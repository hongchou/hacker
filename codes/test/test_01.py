import numpy as np
import matplotlib.pyplot as plt
import time

dx = 0.1
dy = 0.1
dx2 = dx * dx
dy2 = dy * dy


def laplace(u):
    nx, ny = u.shape  # 查看数组的维数
    for i in range(1, nx - 1):
        for j in range(1, ny - 1):
            u[i, j] = ((u[i + 1, j] + u[i - 1, j]) * dy2 + (u[i, j + 1] + u[i, j - 1] * dx2)) / (2 * (dx2 + dy2))


def mat_laplace(u):
    u[1:-1, 1:-1] = ((u[2:, 1:-1] + u[:-2, 1:-1]) * dy2 + (u[1:-1, 2:] + u[1:-1, :-2]) * dx2) / (2 * (dx2 + dy2))


def calc(N, Niter):
    u = np.zeros([N, N])  # 全局初始化
    u[0] = 1  # 边界条件
    for i in range(Niter):
        laplace(u)
    return u


def mat_calc(N, Niter):
    u = np.zeros([N, N])  # 全局初始化
    u[0] = 1  # 边界条件
    for i in range(Niter):
        mat_laplace(u)
    return u


# 运行测试
start = time.clock()
u = calc(100, 3000)
end = time.clock()
time_elapsed = end - start  # 普通计算耗时

start = time.clock()
u = mat_calc(100, 3000)
end = time.clock()
time_elapsed1 = end - start  # 普通计算耗时

plt.pcolormesh(u)
plt.show()
plt.legend()
print(time_elapsed)
print(time_elapsed1)
