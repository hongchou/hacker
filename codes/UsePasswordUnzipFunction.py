import zipfile
import time


def extractFile(zip_file, password):
    try:
        zf = zipfile.ZipFile(zip_file)
        zf.extractall(pwd=bytes(password, 'utf-8'))
        return password
    except Exception as e:
        return
    # finally:
    #     print("没有匹配到密码。。。。")


def main():
    password_file = open('number_pass3.txt')
    # start_time = time.perf_counter()
    start_time = time.process_time()
    for line in password_file.readlines():
        password = line.strip('\n')
        #         破解文件的返回结果
        result = extractFile('test.zip', password)
        # print(str(result)+"-------------")
        if result:
            end_time = time.process_time()
            print(end_time - start_time)
            print('result:' + result)
            exit(0)


if __name__ == '__main__':
    main()
